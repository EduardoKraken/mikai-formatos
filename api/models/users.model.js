const sql = require("./db.js");

// constructor
const Users = function(user) {
  this.nombre = user.nombre;
  this.apellidos = user.apellidos;
  this.empresa = user.empresa;
  this.telempresa     = user.telempresa;
  this.telmovil   = user.telmovil;
  this.email   = user.email;
  this.modmaquina    = user.modmaquina;
  this.matserie  = user.matserie;
  this.password     = user.password;
  this.estatus   = user.estatus;
};

// VARIABLES PARA QUERYS

Users.session = (loger,result) =>{
  sql.query("SELECT * FROM usuarios WHERE email = ? AND password = ?"
    ,[loger.email, loger.password],(err, res) => {
    if (err) { 
      console.log("error: ", err); 
      result(err, null);
      return 
    }
    console.log('res',res)
    result(null, res[0])
  });
};

Users.findOne = (userid, result) => {
  sql.query(`SELECT * FROM usuarios WHERE id = ${userid}`, (err, res) => {
    if (err) {
      console.log("error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      console.log("found user: ", res[0]);
      result(null, res[0]);
      return;
    }
    // not found Customer with the id
    result({ kind: "not_found" }, null);
  });
};


module.exports = Users;