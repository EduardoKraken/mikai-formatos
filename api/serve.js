// IMPORTAR DEPENDENCIAS
const express    = require("express");
const bodyParser = require("body-parser");
var   cors       = require('cors');

// IMPORTAR EXPRESS
const app = express();

//Para servidor con ssl
var http = require('http');
const https = require('https');
const fs = require('fs');

// IMPORTAR PERMISOS
app.use(cors());
// parse requests of content-type: application/json
app.use(bodyParser.json());
// parse requests of content-type: application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// ----IMPORTAR RUTAS---------------------------------------->
var r_users          = require('./routes/users.routes');
var r_formatos       = require('./routes/formatos.routes');

r_users(app);
r_formatos(app);

// ----FIN-DE-LAS-RUTAS-------------------------------------->

// DEFINIT PUERTO EN EL QUE SE ESCUCHARA
// app.listen(3007, () => {
//   console.log("|****************** S-O-F-S-O-L-U-T-I-O-N *********************| ");
//   console.log("|******************** C-R-E-A-T-I-B-E *************************| ");
//   console.log("|**************************************************************| ");
//   console.log("|************ Servidor Corriendo en el Puerto 3007 ************| ");
// });


// //Para servidor con ssl

https.createServer({
    key: fs.readFileSync('/etc/letsencrypt/live/support-smipack.com/privkey.pem'),
    cert: fs.readFileSync('/etc/letsencrypt/live/support-smipack.com/fullchain.pem'),
    passphrase: 'desarrollo'
}, app)
.listen(3007);
