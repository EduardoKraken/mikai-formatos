import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import rutas from '@/router'

import Login      from '@/views/Login.vue'
import Home       from '@/views/Home.vue'
import Formatos   from '@/views/Formatos.vue'

// Reportes


Vue.use(VueRouter)

// const routes: 

const router = new VueRouter({
  mode: '',
  base: process.env.BASE_URL,
  routes: [

    //MODULO DE LOGIN
    { path: '/login/:id', name: 'login' , component: Login, 
      meta: { libre: true}},
    { path: '/home', name: 'home' , component: Home, 
      meta: { ADMIN: true, USUARIO: true}},
    { path: '/formatos', name: 'formatos' , component: Formatos, 
      meta: { ADMIN: true, USUARIO: true }},
  ]
})

router.beforeEach( (to, from, next) => {
  // console.log('Entro',store.state.Login.datosUsuario)

  if(to.matched.some(record => record.meta.libre)){
    next()
  }else if(store.state.Login.datosUsuario.admin === 'ADMIN'){
    if(to.matched.some(record => record.meta.ADMIN)){
      next()
    }
  }else if(store.state.Login.datosUsuario.admin === 'USUARIO' || store.state.Login.datosUsuario.admin === 'SUPERVISOR'){
    if(to.matched.some(record => record.meta.USUARIO)){
      next()
    }
  }else{
    next({
      name: 'login'
    })
  }
})

export default router
